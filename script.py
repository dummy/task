#!/usr/bin/env python
import os
import json

items_file = os.environ["ITEMS_FILE"]
assert os.path.isfile(items_file)

index = os.environ["INDEX"]
assert index.isdigit()
index = int(index)

with open(items_file) as user_file:
  jobs = json.load(user_file)
  assert index < len(jobs)
  job = jobs[index]
  print(f"Hello from task#{index}, job is: {job}")
